
package dealer;

public class PentruFamilie extends Berlina{
    
    int locuri;
    String usi;
    
    public PentruFamilie(String marca, String model, double pret, int locuri, String usi){
        
        this.marca = marca;
        this.model = model;
        this.pret = pret;
        this.locuri = locuri;
        this.usi = usi;
    }
}
