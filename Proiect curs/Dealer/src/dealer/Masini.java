package dealer;

import java.util.*;

public class Masini {

    Scanner input = new Scanner(System.in);
    String marca;
    String model;
    double pret;

    public void testeazaCumpara() {

        ArrayList<Berlina> berl = new ArrayList();
        Berlina seat = new Berlina("Seat", "Leon", 22000);
        Berlina ren = new Berlina("Renault", "Clio", 18000);
        Berlina merc = new Berlina("Mercedes", "Clasa A", 35000);
        Berlina dacia = new Berlina("Dacia", "Logan", 15000);
        Berlina fiat = new Berlina("Fiat", "Panda", 20000);
        berl.add(seat);
        berl.add(ren);
        berl.add(merc);
        berl.add(dacia);
        berl.add(fiat);

        ArrayList<SUV> suv = new ArrayList();
        SUV cay = new SUV("Porche", "Cayenne", 120000, "4x4");
        SUV x1 = new SUV("BMW", "X5", 60000, "2x2");
        SUV x6 = new SUV("BMW", "X6", 150000, "4x4");
        SUV q5 = new SUV("Audi", "Q5", 100000, "2x2");
        SUV q7 = new SUV("Audi", "Q7", 90000, "4x4");
        suv.add(cay);
        suv.add(x1);
        suv.add(x6);
        suv.add(q5);
        suv.add(q7);

        ArrayList<SUVE> suve = new ArrayList();
        SUVE caye = new SUVE("Porche", "Cayenne", 120000, "4x4", "electrica");
        SUVE x1e = new SUVE("BMW", "X5", 60000, "2x2", "hybrid");
        SUVE x6e = new SUVE("BMW", "X6", 150000, "4x4", "electrica");
        SUVE q5e = new SUVE("Audi", "Q5", 100000, "2x2", "hidrogen lichid");
        SUVE q7e = new SUVE("Audi", "Q7", 90000, "4x4", "putere solara");
        suve.add(caye);
        suve.add(x1e);
        suve.add(x6e);
        suve.add(q5e);
        suve.add(q7e);

        ArrayList<Microbuz> micr = new ArrayList();
        Microbuz vito = new Microbuz("Mercedes", "Vito", 120000);
        Microbuz sprint = new Microbuz("Mercedes", "Sprinter", 150000);
        Microbuz transit = new Microbuz("Ford", "Transit", 60000);
        Microbuz vivaro = new Microbuz("Opel", "Vivaro", 100000);
        Microbuz trafic = new Microbuz("Renault", "Trafic", 90000);
        micr.add(vito);
        micr.add(sprint);
        micr.add(transit);
        micr.add(vivaro);
        micr.add(trafic);

        ArrayList<Tir> tir = new ArrayList();
        Tir iveco = new Tir("Iveco", "S-Say", 120000);
        Tir scania = new Tir("Scania", "Hybrid", 60000);
        Tir volvo = new Tir("Volvo", "Globetrotter", 150000);
        Tir daf = new Tir("DAF", "Icepiercer", 100000);
        Tir man = new Tir("MAN", "The Destroyer", 90000);
        tir.add(iveco);
        tir.add(scania);
        tir.add(volvo);
        tir.add(daf);
        tir.add(man);
        
        ArrayList<PentruFamilie> fam = new ArrayList();
        PentruFamilie yaris = new PentruFamilie("Toyota", "Yaris", 23000, 7, "glisante");
        PentruFamilie avensis = new PentruFamilie("Toyota", "Avensis", 30000, 7, "fluture");
        PentruFamilie future = new PentruFamilie("Ford", "Future", 40000, 9, "glisante");
        PentruFamilie nissan = new PentruFamilie("Nissan", "Ultimate", 37000, 5, "");
        PentruFamilie almera = new PentruFamilie("Seat", "Almera", 20000, 7, "glisante");
        fam.add(yaris);
        fam.add(avensis);
        fam.add(future);
        fam.add(nissan);
        fam.add(almera);
        

        System.out.println("\nCe tip de vehicul Va intereseaza? ");
        System.out.println("Alege 1 - Berlina");
        System.out.println("Alege 2 - SUV");
        System.out.println("Alege 3 - Microbuz");
        System.out.println("Alege 4 - Tir");
        System.out.println("Alege: ");
        int tipVehicul = input.nextInt();

        switch (tipVehicul) {
            case 1:
                System.out.println("\nCe masina Va intereseaza? ");
                for (int i = 0; i < berl.size(); i++) {

                    System.out.println("Alege " + (i+1) + " - " + berl.get(i).marca + " " + berl.get(i).model);
                }
                System.out.println("Alegere: ");

                int alegeB = input.nextInt();

                setMarcaAleasa(berl.get(alegeB - 1).marca);
                setModelAles(berl.get(alegeB - 1).model);
                setPretAles(berl.get(alegeB - 1).pret);
                break;

            case 2:
                System.out.println("\nCe SUV Va intereseaza? ");
                System.out.println("Alege 1 - Alimentatie normala");
                System.out.println("Alege 2 - Alimentatie alternativa");
                System.out.print("Alege: ");

                int alimentatie = input.nextInt();

                switch (alimentatie) {
                    case 1:
                        for (int i = 0; i < suv.size(); i++) {
                            System.out.println("Alege " + (i+1) + " - " + suv.get(i).marca + " " + suv.get(i).model + " cu tractiune " + suv.get(i).tractiune);
                        }
                        System.out.println("Alegere: ");

                        int alegeS = input.nextInt();

                        setMarcaAleasa(suv.get(alegeS - 1).marca);
                        setModelAles(suv.get(alegeS - 1).model + " " + suv.get(alegeS - 1).tractiune);
                        setPretAles(suv.get(alegeS - 1).pret);
                        break;
                        
                    case 2:
                        for (int i = 0; i < suve.size(); i++) {
                            System.out.println("Alege " + (i+1) + " - " + suve.get(i).marca + " " + suve.get(i).model + " cu tractiune " + suve.get(i).tractiune + " si alimentatie " + suve.get(i).alimentatie);
                        }
                        System.out.println("Alegere: ");

                        int alegeSE = input.nextInt();

                        setMarcaAleasa(suve.get(alegeSE - 1).marca);
                        setModelAles(suve.get(alegeSE - 1).model + " " + suve.get(alegeSE - 1).tractiune + " " + suve.get(alegeSE - 1).alimentatie );
                        setPretAles(suve.get(alegeSE - 1).pret);
                        break;
                }
                break;

            case 3:
                System.out.println("\nCe Microbuz Va intereseaza? ");

                for (int i = 0; i < micr.size(); i++) {
                    System.out.println("Alege " + (i+1) + " - " + micr.get(i).marca + " " + micr.get(i).model);
                }
                System.out.println("Alegere: ");

                int alegeM = input.nextInt();

                setMarcaAleasa(micr.get(alegeM - 1).marca);
                setModelAles(micr.get(alegeM - 1).model);
                setPretAles(micr.get(alegeM - 1).pret);
                break;

            case 4:
                System.out.println("\nCe TIR Va intereseaza? ");

                for (int i = 0; i < tir.size(); i++) {
                    System.out.println("Alege " + (i+1) + " - " + tir.get(i).marca + " " + tir.get(i).model);
                }
                System.out.println("Alegere: ");

                int alegeT = input.nextInt();

                setMarcaAleasa(tir.get(alegeT - 1).marca);
                setModelAles(tir.get(alegeT - 1).model);
                setPretAles(tir.get(alegeT - 1).pret);
                break;
        }
    }

    public void claxon() {
        System.out.println(" iese conducand si claxoneaza frenetic de fericire pentru ca si-a luat "
                + getMarcaAleasa() + " " + getModelAles() + " nou nouta!!!!!");
    }

    public String getMarcaAleasa() {
        return marca;
    }

    public void setMarcaAleasa(String marca) {
        this.marca = marca;
    }

    public String getModelAles() {
        return model;
    }

    public void setModelAles(String model) {
        this.model = model;
    }

    public void setPretAles(double pret) {
        this.pret = pret;
    }

    public double getPretAles() {
        return pret;
    }
}
