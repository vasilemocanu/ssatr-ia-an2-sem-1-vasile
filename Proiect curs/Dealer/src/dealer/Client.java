package dealer;

import java.util.*;

public class Client {
    
    Masini masini = new Masini();
    Scanner input = new Scanner(System.in);
    Scanner cuvant = new Scanner(System.in);
    String nume;
    int varsta;
    int buget = 0;
    String da = "DA";
    String nu = "NU";
   

    public void cumpara() {
               
        masini.testeazaCumpara();  // cls Masini
        getBugetInfo();
                
        System.out.println("\nMasina " + masini.getMarcaAleasa() + " " + masini.getModelAles() + " pe care ati ales-o costa " + masini.getPretAles());

        if (getBuget() >= masini.getPretAles()) {
            System.out.println("\nAti facut o alegere potrivita, este perfecta pentru Dvs!");
            System.out.print(getNume()); masini.claxon();
            
        } else {
            System.out.println("\nDin pacate pretul masinii este mai ridicat, va putem oferi un imprumut pentru restul sumei.");
        }
    }

    public void testeaza() {

        masini.testeazaCumpara();  // cls Masini

        System.out.println("\n" + getNume() + " merge la plimbare cu " + masini.getMarcaAleasa() + " " + masini.getModelAles());
        System.out.println("... dupa trecerea timpului ...");
        System.out.println("\nVa place? Vreti sa stiti mai multe detalii, configuratia accesoriilor si pretul? da/nu");

        String raspuns = cuvant.next();

        if (raspuns.equalsIgnoreCase(da)) {
            System.out.println("\nVa rugam intrati in birou sa discutam in detaliu.");
            System.out.println(getNume() + " intra in birou.");
            System.out.println("... dupa trecerea timpului ...");
            System.out.println("\nSunteti hotarat sa o cumparati? da/nu");
            
            raspuns = cuvant.next();

            if (raspuns.equalsIgnoreCase(da)) {
                getBugetInfo();
                if (getBuget() >= masini.getPretAles()) {
                    System.out.println("\nAti facut o alegere potrivita, este perfecta pentru Dvs!");
                    System.out.print(getNume()); masini.claxon();
                } else {
                    System.out.println("\nDin pacate pretul de vanzare este mai ridicat, va putem oferi un imprumut pentru restul sumei.");
                }
            } else if (raspuns.equalsIgnoreCase(nu)) {
                System.out.println("\nNe pare rau ca nu am cazut la o intelegere. Va dorim o zi buna.");
                System.out.println(getNume() + " pleaca acasa pe jos.");
            }

        } else if (raspuns.equalsIgnoreCase(nu)) {
            System.out.println("\nNe pare rau ca nu v-a incantat test drive-ul, speram sa reveniti. La revedere!");
            System.out.println(getNume() + " pleaca acasa pe jos.");
        }
    }

    public void alegeNume() {
        System.out.println("Buna ziua! Cum va numiti?: ");
        nume = input.nextLine();
        setNume(nume);
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public void getBugetInfo() {
        System.out.println("\nCare va este bugetul?: ");
        int suma = input.nextInt();

        if (suma > 0) {
            setBuget(suma);
//            System.out.println("\nBugetul clientului este: " + getBuget());
        } else {
            System.out.println("\nScrieti o suma valabila: ");
            getBugetInfo();
        }
    }

    public void setBuget(int buget) {
        this.buget = buget;
    }

    public int getBuget() {
        return buget;
    }

    public void getAge() {
        System.out.println("\nCe varsta aveti?");
        int varsta = input.nextInt();
        setVarsta(varsta);
        if (varsta < 18) {
            System.out.println("\nNe pare rau, dar trebuie sa fiti major pentru a putea testa sau cumpara o masina.");
        }
        else if (varsta > 70) {
            System.out.println("\nNe pare rau, ati depasit varsta acceptata pentru a putea testa sau cumpara o masina.");
        }
    }

    public int getVarsta() {
        return varsta;
    }

    public void setVarsta(int varsta) {
        this.varsta = varsta;
    }

}
