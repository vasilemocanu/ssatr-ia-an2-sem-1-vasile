package dealer;

import java.io.*;
import java.util.*;

public class Dealer {    
    
    public static void main(String[] args) throws IOException {
        
        Client client = new Client();
        Scanner input = new Scanner(System.in);

        client.alegeNume();

        System.out.println("\nBine ai venit " + client.getNume() + ", esti interesat sa cumperi sau doar sa te uiti? Alege varianta potrivita:");
        System.out.println("Alege 1 - Vreau doar sa vad ce aveti pe aici");
        System.out.println("Alege 2 - Vreau sa testez");
        System.out.println("Alege 3 - Sunt interesat sa cumpar");
        
        System.out.print("Alege: ");

        int alege = input.nextInt();

        switch (alege) {

            case 1:
                System.out.println("\nVa dorim plimbare placuta, sa ne anuntati daca va razganditi.\n");
                break;

            case 2:
                client.getAge();
                if (client.getVarsta() >= 18 && client.getVarsta() <= 70) {
                    client.testeaza();
                }
                break;

            case 3:
                client.getAge();
                if (client.getVarsta() >= 18 && client.getVarsta() <= 70) {
                    client.cumpara();
                }
                break;
                
            case 4:
                // cu asta doar fac eu probe
                break;
        }
    }
}