package vasile.mocanu.lab2.ex7;

import java.util.Scanner;

public class NewClass {

    public static void main(String[] args) {

        int i = 0;
        double a;
        double x = Math.random();

        Scanner scan = new Scanner(System.in);
        System.out.print("Try and guess the number: ");
        a = scan.nextDouble();

        while (i < 3) {

            if (a == x) {
                System.out.print("Congratulations, you guessed correctly!");
                break;

            } else {

                if (a < x) {
                    System.out.print("Wrong answer, your number it too low, guess again: ");
                } else if (a > x) {
                    System.out.print("Wrong answer, your number it too high, guess again: ");
                }
                a = scan.nextDouble();
            }
            i++;
        }

        if (i == 3 && a != x) {
            System.out.print("You lost!");
        }
    }
}
