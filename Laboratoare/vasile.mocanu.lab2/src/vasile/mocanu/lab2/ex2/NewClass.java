package vasile.mocanu.lab2.ex2;

import java.util.Scanner;

public class NewClass {
    
    public static void main(String[] args){
        
        int x;
        
        Scanner scan = new Scanner(System.in);
        System.out.print("Introduceti un numar intreg: ");
        
        x = scan.nextInt();       
        
        if (x==1){            
            System.out.println("UNU");
        }
        
        else if(x==2){
            System.out.println("DOI");
        }
        
        else if(x==3){
            System.out.println("TREI");
        }
        
        else if(x==4){
            System.out.println("PATRU");
        }
        
        else if(x==5){
            System.out.println("CINCI");
        }
        
        else if(x==6){
            System.out.println("SASE");
        }
        
        else if(x==7){
            System.out.println("SAPTE");
        }
        
        else if(x==8){
            System.out.println("OPT");
        }
        
        else if(x==9){
            System.out.println("NOUA");
        }
        
        else{
            System.out.println("OTHER");
        }    
        
    }
}
